# Reverse Tunnel

This is a small demo of using the Go *http2* package
to send and receive HTTP/2 messages over an already established
TCP connection.

The 'client' half dials the server, and then listens for
incoming HTTP/2 requests on the open connection and handles
the requests and sends a basic response.

The 'server' half listens for incoming client-tunnels on
port 8080, but then also has an HTTP server on port 8081.
Requests on port 8081 are then forwarded down the tunnel.

## Building

```
make
```

## Running

Starting the server:

```
bin/main server
```

Connect one or more clients:

```
bin/main client
```

Send requests to the server's http endpoint:

```
curl http://localhost:8081
```

The response printed to the terminal is the demo-response
from the http-handler in *client*.
