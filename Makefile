
build:
	go build -o bin/main .
.PHONY: build

clean:
	rm -rf bin
.PHONY: clean
