package main

import (
	"context"
	"fmt"
	"os"
	"os/signal"
	"slices"
	"strings"
)

func main() {
	err := mainErr()
	if err != nil {
		fmt.Fprintln(os.Stderr, "error:", err)
		os.Exit(1)
	}
}

func mainErr() error {
	args := os.Args[1:]
	var cmdName string
	var cmdPos int
	for i, arg := range args {
		if arg == "--" {
			break
		}
		if strings.HasPrefix(arg, "-") {
			continue
		}
		cmdName = arg
		cmdPos = i
		break
	}
	if cmdName == "" {
		return fmt.Errorf("command is required")
	}
	args = slices.Delete(args, cmdPos, cmdPos+1)

	cmd, ok := commands[cmdName]
	if !ok {
		return fmt.Errorf("unknown command %q", cmdName)
	}

	ctx, cancel := signal.NotifyContext(context.Background(), os.Interrupt)
	defer cancel()

	return cmd(ctx, args)
}

var commands = map[string]func(context.Context, []string) error{
	"client": client,
	"server": server,
}
