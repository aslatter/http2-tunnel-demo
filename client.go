package main

import (
	"context"
	"crypto/tls"
	"flag"
	"fmt"
	"net"
	"net/http"
	"strings"
	"sync"

	"golang.org/x/net/http2"
)

func client(ctx context.Context, args []string) error {
	var notls bool
	var addr string

	fmt.Println(strings.Join(args, " "))

	flag.StringVar(&addr, "addr", "localhost:8080", "address to connect to")
	flag.BoolVar(&notls, "notls", false, "disable TLS")
	flag.CommandLine.Parse(args)

	// the client opens a connection to the server and then
	// starts an http2 server on the open connection.

	conn, err := dial(ctx, addr, notls)
	if err != nil {
		return fmt.Errorf("dialing %s: %s", addr, err)
	}
	defer conn.Close()

	// at this point we could do some raw protocol to auth
	// or something?

	http2.VerboseLogs = true

	var s http2.Server
	var wg sync.WaitGroup

	wg.Add(1)
	go func() {
		defer wg.Done()
		<-ctx.Done()
		conn.Close()
	}()

	s.ServeConn(conn, &http2.ServeConnOpts{
		Handler: http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			// we could have a more complex handler here, which examined the incoming
			// authority to handle different 'types' of messages.
			fmt.Fprintln(w, "hello, world!")
		}),
	})

	return nil
}

func dial(ctx context.Context, addr string, notls bool) (net.Conn, error) {
	if notls {
		var dialer net.Dialer
		return dialer.DialContext(ctx, "tcp", addr)
	}

	var dialer tls.Dialer
	dialer.Config = &tls.Config{
		InsecureSkipVerify: true,
	}

	return dialer.DialContext(ctx, "tcp", addr)
}
