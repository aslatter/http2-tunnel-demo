package main

import (
	"net/http"
	"time"

	"golang.org/x/net/http2"
)

type connPool struct {
	p chan *http2.ClientConn
}

func newConnPool(size int) *connPool {
	if size < 1 {
		size = 10
	}
	return &connPool{p: make(chan *http2.ClientConn, size)}
}

func (c *connPool) insert(conn *http2.ClientConn) bool {
	select {
	case c.p <- conn:
		return true
	case <-time.After(time.Second):
		return false
	}
}

func (c *connPool) pooledConn() *http2.ClientConn {
	// TODO - timeout?
	for {
		conn := <-c.p

		st := conn.State()
		if st.Closed || st.Closing {
			continue
		}

		select {
		case c.p <- conn:
		default:
		}

		return conn
	}
}

// RoundTrip implements http.RoundTripper.
func (c *connPool) RoundTrip(r *http.Request) (*http.Response, error) {
	return c.pooledConn().RoundTrip(r)
}

var _ http.RoundTripper = (*connPool)(nil)
