package main

import (
	"context"
	"crypto/tls"
	"flag"
	"fmt"
	"io"
	"net"
	"net/http"
	"sync"

	"golang.org/x/net/http2"
)

func server(ctx context.Context, args []string) error {
	http2.VerboseLogs = true

	var notls bool
	var addr1 string
	var addr2 string

	flag.StringVar(&addr1, "addr1", "localhost:8080", "address tunnels connect to")
	flag.StringVar(&addr2, "addr2", "localhost:8081", "address clients connect to")
	flag.BoolVar(&notls, "notls", false, "disable TLS")
	flag.CommandLine.Parse(args)

	// the server listens on two ports:
	// + addr1 receives incoming tunnels
	// + addr2 is an http server, forwarding requests down the tunnel
	//
	// Each accepted request on the tunnel-port gets thrown into out
	// 'http client connection pool', which are used in the http-server
	// handler to make requests down the tunnel.
	//
	// The tunnel is implemented with http2, because it supports multiple
	// streams and because we have existing libraries for framing http
	// requests and responses onto it - but it really could be anything.

	listener, err := listen(addr1, notls)
	if err != nil {
		return fmt.Errorf("creating listener on %s: %s", addr1, err)
	}

	pool := newConnPool(10)

	srv := http.Server{
		Addr: addr2,
		Handler: &tunnelServer{
			proxyClient: &http.Client{
				Transport: pool,
			},
		},
	}

	ctx, cancel := context.WithCancel(ctx)
	defer cancel()
	var wg sync.WaitGroup

	wg.Add(1)
	go func() {
		defer wg.Done()
		srv.ListenAndServe()
	}()

	wg.Add(1)
	go func() {
		defer wg.Done()
		<-ctx.Done()
		_ = listener.Close()
		srv.Close()
	}()

	var acceptErr error

	for {
		var conn net.Conn
		conn, acceptErr = listener.Accept()
		if acceptErr != nil {
			if ctx.Err() != nil {
				break
			}
		}

		var t http2.Transport
		clientConn, err := t.NewClientConn(conn)
		if err != nil {
			fmt.Println("error creating http2 client conn:", err)
			conn.Close()
			continue
		}

		if !pool.insert(clientConn) {
			clientConn.Close()
			conn.Close()
		}
	}

	wg.Wait()

	return acceptErr
}

func listen(addr string, notls bool) (net.Listener, error) {
	if notls {
		return net.Listen("tcp", addr)
	}

	cert, err := generateCert()
	if err != nil {
		return nil, fmt.Errorf("generating server cert: %s", err)
	}

	return tls.Listen("tcp", addr, &tls.Config{
		Certificates: []tls.Certificate{*cert},
	})
}

type tunnelServer struct {
	proxyClient *http.Client
}

func (t *tunnelServer) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	// we must have a scheme and authority on the outbound
	// http2 request.

	proxyUrl := *r.URL
	proxyUrl.Scheme = "https"
	proxyUrl.Host = "example"

	proxyRequest, err := http.NewRequestWithContext(r.Context(), r.Method, proxyUrl.String(), r.Body)
	if err != nil {
		w.WriteHeader(500)
		fmt.Fprintln(w, err)
		return
	}
	proxyRequest.Header = r.Header.Clone()

	proxyResponse, err := t.proxyClient.Do(proxyRequest)
	if err != nil {
		w.WriteHeader(500)
		fmt.Fprintln(w, err)
		return
	}
	// TODO - set response headers
	io.Copy(w, proxyResponse.Body)
}

var _ http.Handler = (*tunnelServer)(nil)
